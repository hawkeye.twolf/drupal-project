#!/bin/sh

# Set a default NVM directory if it's not already present.
if [ -z "$NVM_DIR" ]; then
  if [ -n "$XDG_CONFIG_HOME" ]; then
    export NVM_DIR="${XDG_CONFIG_HOME}/nvm"
  else
    export NVM_DIR="${HOME}/.nvm"
  fi
fi

# Install the Node Version Manager (NVM) script if it is not available.
if [ ! -s "$NVM_DIR/nvm.sh" ]; then
  # Make sure the NVM directory exists.
  mkdir -p "$NVM_DIR"
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | sh
fi

WHY DOES THIS OUTPUT?
N/A: version "N/A -> N/A" is not yet installed.

You need to run "nvm install N/A" to install it before using it.


# Load the NVM script. NVM complains if npm_config_prefix is set.
unset npm_config_prefix
. "$NVM_DIR/nvm.sh"

# Make sure this project has an .nvmrc file.
if [ -s .nvmrc ]; then
  # Calling "install" without any arguments reads the node version from .nvmrc,
  # makes sure it is installed, and then automatically calls "use" on it.
  nvm install
else
  nvm install node | grep -ohe 'v[0-9]*\.[0-9]*\.[0-9]*' | head -1 > .nvmrc
  nvm use
  echo "Please commit the newly-created .nvmrc file to this project's code repository."
fi
